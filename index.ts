import { GameFactory } from './oop/core/GameFactory';
import { GameFactoryV0 } from './oop/v0-simple/GameFactoryV0';
import { GameFactoryV1 } from './oop/v1-inheritance/GameFactoryV1';
import { GameFactoryV2 } from './oop/v2-more-inheritance/GameFactoryV2';
import { GameFactoryV3 } from './oop/v3-composition/GameFactoryV3';
import { GameFactoryV4 } from './oop/v4-decomposition/GameFactoryV4';
import { GameFactoryV5 } from './oop/v5-configuration/GameFactoryV5';
import { GameFactoryV6 } from './oop/v6-srp/GameFactoryV6';

const { UNITS_COUNT = '5', VERSION } = process.env;

const factory = createFactory(VERSION)
const game = factory.createGame({ unitsCount: Number(UNITS_COUNT) })

game.start();

function createFactory(version?: string): GameFactory {
    switch (version) {
        case '0':
            return new GameFactoryV0();
        case '1':
            return new GameFactoryV1();
        case '2':
            return new GameFactoryV2();
        case '3':
            return new GameFactoryV3();
        case '4':
            return new GameFactoryV4();
        case '5':
            return new GameFactoryV5();
        case '6':
            return new GameFactoryV6();
        default:
            return new GameFactoryV1();
    }
}
