import { specialtyConfig, SpecialtyConfigData } from '../../config';
import { Attack } from '../attack/Attack';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { Character } from '../Character';
import { Stats } from '../Stats';
import { SpecialtyName } from './SpecialtyName';

export class Specialty {
    public readonly name: SpecialtyName;

    constructor(name: SpecialtyName) {
        this.name = name;
    }

    public createStats(): Stats {
        return new Stats(this.config.stats);
    }

    public createAttack(target: Character, stats: Stats, modifiers: AttackModifier[]): Attack {
        const { attackCtor } = this.config;

        return new attackCtor(target, stats, modifiers);
    }

    public toString(): string {
        return this.name;
    }

    private get config(): SpecialtyConfigData {
        return specialtyConfig[this.name];
    }
}
