import { raceConfig, RaceConfigData } from '../../config';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { RaceName } from './RaceName';

export class Race {
    public readonly name: RaceName;

    constructor(name: RaceName) {
        this.name = name;
    }

    public getAttackModifiers(): AttackModifier[] {
        return this.config.attackModifiers;
    }

    public isElf(): boolean {
        return this.name === RaceName.Elf;
    }

    public isOrc(): boolean {
        return this.name === RaceName.Orc;
    }

    public isDwarf(): boolean {
        return this.name === RaceName.Dwarf;
    }

    public toString(): string {
        return this.name;
    }

    private get config(): RaceConfigData {
        return raceConfig[this.name];
    }
}
