interface Params {
    hp: number;
    strength: number;
    stamina: number;
    dexterity: number;
    intelligence: number;
}

export class Stats {
    public hp: number = 500;
    public strength: number = 5;
    public stamina: number = 5;
    public dexterity: number = 5;
    public intelligence: number = 5;

    constructor(params: Params) {
        this.hp = params.hp;
        this.strength = params.strength;
        this.stamina = params.stamina;
        this.dexterity = params.dexterity;
        this.intelligence = params.intelligence;
    }

    public get defense(): number {
        return this.dexterity * 5 + this.stamina;
    }

    public decreaseHpBy(damage: number): void {
        this.hp = this.hp > damage ? this.hp - damage : 0;
    }

    public calculateHp(level: number): number {
        return level * 50 + this.stamina * 10;
    }

    public isAlive(): boolean {
        return this.hp > 0;
    }

    public isDead(): boolean {
        return !this.isAlive();
    }
}
