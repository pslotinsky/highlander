import { faker } from '@faker-js/faker';
import assert from 'assert';

import { Unit } from '../../core/Game';
import { Attack } from './attack/Attack';
import { AttackModifier } from './attack/modifiers/AttackModifier';
import { Level } from './Level';
import { Race } from './race/Race';
import { Specialty } from './specialty/Specialty';
import { Stats } from './Stats';

export class Character implements Unit {
    protected name: string = faker.name.fullName();
    protected level: Level;
    protected stats: Stats;
    protected race: Race;
    protected specialty: Specialty;

    constructor(race: Race, specialty: Specialty) {        
        this.race = race;
        this.specialty = specialty;
        this.stats = specialty.createStats();
        this.level = new Level(this.stats);

        console.info(`${this} created`);
    }

    public attack(victim: Character): void {
        console.info(`${this} attacked ${victim}`);

        assert(this.isAlive(), `Dead character can't attack`);
        assert(victim.isAlive(), `Dead character can't be attacked`);

        const attack = this.createAttack(victim);
        attack.execute();

        if (victim.isDead()) {
            this.onKill(victim);
        }
    }

    public isAlive(): boolean {
        return this.stats.isAlive();
    }

    public isDead(): boolean {
        return this.stats.isDead();
    }

    public isDwarf(): boolean {
        return this.race.isDwarf();
    }

    public isElf(): boolean {
        return this.race.isElf();
    }

    public isOrc(): boolean {
        return this.race.isOrc();
    }

    public suffer(attack: Attack): void {
        attack.reduceBy(this.stats);

        console.info(`${this} suffered ${attack.damage} hp damage`);

        assert(attack.damage >= 0, `Can't suffer negative damage`);

        this.stats.decreaseHpBy(attack.damage);
    }

    public toString(): string {
        return `${this.race}-${this.specialty} ${this.name} (level ${this.level})`;
    }

    protected createAttack(victim: Character): Attack {
        const modifiers = this.getAttackModifiers();

        return this.specialty.createAttack(victim, this.stats, modifiers);
    };

    protected getAttackModifiers(): AttackModifier[] {
        return this.race.getAttackModifiers();
    }

    protected onKill(victim: Character): void {
        console.info(`${victim} killed`);

        const exp = this.level.calculateExpForKilling(victim.level);
        this.level.addExp(exp);
    }
}
