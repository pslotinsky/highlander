import { Character } from '../Character';
import { Stats } from '../Stats';
import { AttackModifier } from './modifiers/AttackModifier';

export abstract class Attack {
    public damage: number;

    protected target: Character;
    protected stats: Stats;
    protected modifiers: AttackModifier[];

    constructor(target: Character, stats: Stats, modifiers: AttackModifier[]) {
        this.target = target;
        this.stats = stats;
        this.damage = this.getRawDamage();
        this.modifiers = modifiers;
    }

    public execute(): void {
        for (const modifier of this.modifiers) {
            if (modifier.isApplicableFor(this.target)) {
                this.damage = modifier.applyOn(this.damage);
            }
        }

        this.target.suffer(this);
    }

    public reduceBy(stats: Stats): void {
        this.damage = this.damage > stats.defense ? Math.round(this.damage - stats.defense) : 0;
    }

    public abstract getRawDamage(): number;
}
