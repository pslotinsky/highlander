import { Character } from '../../Character';
import { AttackModifier } from './AttackModifier';

export class AntiElfAttackModifier extends AttackModifier {
    public isApplicableFor(target: Character): boolean {
        return target.isElf();
    }

    public applyOn(damage: number): number {
        return 1.1 * damage;
    }
}
