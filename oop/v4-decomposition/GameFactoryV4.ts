import { faker } from '@faker-js/faker';

import { Unit } from '../core/Game';
import { GameFactory } from '../core/GameFactory';
import { Character } from './character/Character';
import { Dwarf } from './character/race/Dwarf';
import { Elf } from './character/race/Elf';
import { Orc } from './character/race/Orc';
import { Ranger } from './character/specialty/Ranger';
import { Warrior } from './character/specialty/Warrior';
import { Wizard } from './character/specialty/Wizard';

export class GameFactoryV4 extends GameFactory {
    protected createUnit(): Unit {
        const raceCtor = faker.helpers.arrayElement([Dwarf, Elf, Orc]);
        const specialtyCtor = faker.helpers.arrayElement([Ranger, Warrior, Wizard]);
    
        return new Character(new raceCtor(), new specialtyCtor());
    }
}
