import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { AntiOrcAttackModifier } from '../attack/modifiers/AntiOrcAttackModifier';
import { Race, RaceName } from './Race';

export class Dwarf extends Race {
    public name: RaceName = RaceName.Orc;

    public getAttackModifiers(): AttackModifier[] {
        return [new AntiOrcAttackModifier()];
    }
}
