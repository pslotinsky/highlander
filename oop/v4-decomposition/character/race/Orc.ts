import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { Race, RaceName } from './Race';
import { AntiElfAttackModifier } from '../attack/modifiers/AntiElfAttackModifier';

export class Orc extends Race {
    public name: RaceName = RaceName.Orc;

    public getAttackModifiers(): AttackModifier[] {
        return [new AntiElfAttackModifier()];
    }
}
