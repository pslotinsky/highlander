import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { AntiDwarfAttackModifier } from '../attack/modifiers/AntiDwarfAttackModifier';
import { Race, RaceName } from './Race';

export class Elf extends Race {
    public name: RaceName = RaceName.Orc;

    public getAttackModifiers(): AttackModifier[] {
        return [new AntiDwarfAttackModifier()];
    }
}
