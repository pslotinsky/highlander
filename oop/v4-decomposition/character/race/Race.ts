import { AttackModifier } from '../attack/modifiers/AttackModifier';

export enum RaceName {
    Orc = 'Orc',
    Elf = 'Elf',
    Dwarf = 'Dwarf',
}

export abstract class Race {
    public abstract name: RaceName;

    public abstract getAttackModifiers(): AttackModifier[];

    public isElf(): boolean {
        return this.name === RaceName.Elf;
    }

    public isOrc(): boolean {
        return this.name === RaceName.Orc;
    }

    public isDwarf(): boolean {
        return this.name === RaceName.Dwarf;
    }

    public toString(): string {
        return this.name;
    }
}
