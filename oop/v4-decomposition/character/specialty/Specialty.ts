import { Attack } from '../attack/Attack';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { Character } from '../Character';
import { Stats } from '../Stats';

export enum SpecialtyName {
    Ranger = 'Ranger',
    Warrior = 'Warrior',
    Wizard = 'Wizard',
}

export abstract class Specialty {
    public abstract name: SpecialtyName;

    public abstract createStats(): Stats;
    public abstract createAttack(target: Character, stats: Stats, modifiers: AttackModifier[]): Attack;

    public toString(): string {
        return this.name;
    }
}
