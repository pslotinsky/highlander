import { Attack } from '../attack/Attack';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { RangedAttack } from '../attack/RangedAttack';
import { Character } from '../Character';
import { Stats } from '../Stats';
import { Specialty, SpecialtyName } from './Specialty';

export class Ranger extends Specialty {
    public name: SpecialtyName = SpecialtyName.Ranger;

    public createStats(): Stats {
        return new Stats({
            hp: 500,
            strength: 5,
            stamina: 5,
            dexterity: 9,
            intelligence: 5,
        });
    }

    public createAttack(target: Character, stats: Stats, modifiers: AttackModifier[]): Attack {
        return new RangedAttack(target, stats, modifiers);
    }
}
