import { Attack } from '../attack/Attack';
import { MagicAttack } from '../attack/MagicAttack';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { Character } from '../Character';
import { Stats } from '../Stats';
import { Specialty, SpecialtyName } from './Specialty';

export class Wizard extends Specialty {
    public name: SpecialtyName = SpecialtyName.Wizard;

    public createStats(): Stats {
        return new Stats({
            hp: 500,
            strength: 5,
            stamina: 5,
            dexterity: 5,
            intelligence: 9,
        });
    }

    public createAttack(target: Character, stats: Stats, modifiers: AttackModifier[]): Attack {
        return new MagicAttack(target, stats, modifiers);
    }
}
