import { Attack } from '../attack/Attack';
import { MeleeAttack } from '../attack/MeleeAttack';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { Character } from '../Character';
import { Stats } from '../Stats';
import { Specialty, SpecialtyName } from './Specialty';

export class Warrior extends Specialty {
    public name: SpecialtyName = SpecialtyName.Warrior;

    public createStats(): Stats {
        return new Stats({
            hp: 750,
            strength: 9,
            stamina: 7,
            dexterity: 3,
            intelligence: 3,
        });
    }

    public createAttack(target: Character, stats: Stats, modifiers: AttackModifier[]): Attack {
        return new MeleeAttack(target, stats, modifiers);
    }
}
