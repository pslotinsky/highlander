import { Character } from '../../Character';
import { AttackModifier } from './AttackModifier';

export class AntiDwarfAttackModifier extends AttackModifier {
    public isApplicableFor(target: Character): boolean {
        return target.isDwarf();
    }

    public applyOn(damage: number): number {
        return 1.1 * damage;
    }
}
