import { Character } from '../../Character';
import { AttackModifier } from './AttackModifier';

export class AntiOrcAttackModifier extends AttackModifier {
    public isApplicableFor(target: Character): boolean {
        return target.isOrc();
    }

    public applyOn(damage: number): number {
        return 1.1 * damage;
    }
}
