import { Attack } from './Attack';

export class MeleeAttack extends Attack {
    public getRawDamage(): number {
        return this.stats.strength * 10 + this.stats.dexterity * 5;
    }
}
