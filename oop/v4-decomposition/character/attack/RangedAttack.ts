import { Attack } from './Attack';

export class RangedAttack extends Attack {
    public getRawDamage(): number {
        return this.stats.dexterity * 10 + this.stats.strength * 5;
    }
}
