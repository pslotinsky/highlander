import { Attack } from './Attack';

export class MagicAttack extends Attack {
    public getRawDamage(): number {
        return this.stats.intelligence * 5 + this.stats.dexterity * 5 + this.stats.strength * 5;
    }
}
