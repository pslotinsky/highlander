import { faker } from '@faker-js/faker';

import { Unit } from '../core/Game';
import { GameFactory } from '../core/GameFactory';
import { DwarfRanger } from './character/race/dwarf/DwarfRanger';
import { DwarfWarrior } from './character/race/dwarf/DwarfWarrior';
import { DwarfWizard } from './character/race/dwarf/DwarfWizard';
import { ElfRanger } from './character/race/elf/ElfRanger';
import { ElfWarrior } from './character/race/elf/ElfWarrior';
import { ElfWizard } from './character/race/elf/ElfWizard';
import { OrcRanger } from './character/race/orc/OrcRanger';
import { OrcWarrior } from './character/race/orc/OrcWarrior';
import { OrcWizard } from './character/race/orc/OrcWizard';

export class GameFactoryV2 extends GameFactory {
    protected createUnit(): Unit {
        const ctor = faker.helpers.arrayElement([
            DwarfRanger,
            DwarfWarrior,
            DwarfWizard,
            ElfRanger,
            ElfWarrior,
            ElfWizard,
            OrcRanger,
            OrcWarrior,
            OrcWizard,
        ]);
    
        return new ctor();
    }
}
