import { Character } from '../Character';

export abstract class Warrior extends Character {
    protected hp: number = 750;
    protected strength: number = 9;
    protected stamina: number = 7;
    protected dexterity: number = 3;
    protected intelligence: number = 3;

    protected createDamage(_: Character): number {
        return this.strength * 10 + this.dexterity * 5;
    }
}
