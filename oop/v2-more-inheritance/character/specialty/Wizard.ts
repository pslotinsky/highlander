import { Character } from '../Character';

export abstract class Wizard extends Character {
    protected hp: number = 500;
    protected strength: number = 5;
    protected stamina: number = 5;
    protected dexterity: number = 5;
    protected intelligence: number = 9;

    protected createDamage(_: Character): number {
        return this.intelligence * 5 + this.dexterity * 5 + this.strength * 5;
    }
}
