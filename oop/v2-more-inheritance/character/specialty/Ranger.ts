import { Character } from '../Character';

export abstract class Ranger extends Character {
    protected hp: number = 500;
    protected strength: number = 5;
    protected stamina: number = 5;
    protected dexterity: number = 9;
    protected intelligence: number = 5;

    protected createDamage(_: Character): number {
        return this.dexterity * 10 + this.strength * 5;
    }
}
