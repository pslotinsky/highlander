import { Character } from '../../Character';
import { Race } from '../Race';
import { Ranger } from '../../specialty/Ranger';

const DAMAGE_MULTIPLIER = 1.1;

export class OrcRanger extends Ranger {
    protected race: Race = Race.Orc;

    protected createDamage(victim: Character): number {
        const basicDamage = super.createDamage(victim);

        return victim.hasRace(Race.Elf) ? DAMAGE_MULTIPLIER * basicDamage : basicDamage;
    }
}
