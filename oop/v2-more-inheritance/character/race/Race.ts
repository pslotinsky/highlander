export enum Race {
    Orc = 'Orc',
    Elf = 'Elf',
    Dwarf = 'Dwarf',
}
