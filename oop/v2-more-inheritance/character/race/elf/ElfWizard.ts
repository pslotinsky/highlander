import { Character } from '../../Character';
import { Race } from '../Race';
import { Wizard } from '../../specialty/Wizard';

const DAMAGE_MULTIPLIER = 1.1;

export class ElfWizard extends Wizard {
    protected race: Race = Race.Elf;

    protected createDamage(victim: Character): number {
        const basicDamage = super.createDamage(victim);

        return victim.hasRace(Race.Dwarf) ? DAMAGE_MULTIPLIER * basicDamage : basicDamage;
    }
}
