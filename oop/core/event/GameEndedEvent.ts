import { Unit } from '../Game';
import { Event } from './Event';

export class GameEndedEvent extends Event<{ winner: Unit }> {}
