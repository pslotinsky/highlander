import { Event } from './Event';

export class TurnStartedEvent extends Event<{ turn: number }> {}
