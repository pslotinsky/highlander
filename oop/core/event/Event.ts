export class Event<T = any> {
    public readonly data: T;

    constructor(data: T) {
        this.data = data;
    }

    public toString(): string {
        const name = this.constructor.name;
        const data = this.data as any;
        const args = Object.keys(data).map(key => `${key}: ${data[key]}`);

        return [name, ...args].join('\n   ') + '\n';
    }
}
