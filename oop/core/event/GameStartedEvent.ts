import { Event } from './Event';

export class GameStartedEvent extends Event<{ name: string }> {}
