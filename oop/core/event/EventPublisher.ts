import { Event } from './Event';

type Handler<T = any> = (event: Event<T>) => void;

type EventCtor<T> = new (data: T) => Event<T>;

interface Subscription {
    eventName: string;
    handler: Handler;
}

export class EventPublisher {
    private subscriptions: Subscription [] = [];

    public subscribe<T>(eventCtor: EventCtor<T>, handler: Handler<T>): void {
        const eventName = eventCtor.name;
        this.subscriptions.push({ eventName, handler });
    }

    public publish<T>(event: Event<T>): void {
        const eventHandlers = this.getEventHandlers(event.constructor.name);

        for (const handler of eventHandlers) {
            handler(event);

            // console.info(event.toString());
        }
    }

    private getEventHandlers(eventName: string): Handler[] {
        return this.subscriptions
            .filter(subscription => subscription.eventName === eventName)
            .map(subscription => subscription.handler);
    }
}
