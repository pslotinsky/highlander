import { EventPublisher } from './event/EventPublisher';
import { GameEndedEvent } from './event/GameEndedEvent';
import { GameStartedEvent } from './event/GameStartedEvent';
import { TurnStartedEvent } from './event/TurnStartedEvent';

export class EventLogger {
    public subscribeTo(publisher: EventPublisher) {
        publisher.subscribe(GameStartedEvent, this.onGameStarted);
        publisher.subscribe(GameEndedEvent, this.onGameEnded);
        publisher.subscribe(TurnStartedEvent, this.onTurnStarted);
    }

    private onGameStarted(event: GameStartedEvent): void {
        console.info(`\nGame ${event.data.name} started!`);
    }

    private onGameEnded(event: GameEndedEvent): void {
        console.info(`\nGame ended. We have a winner: ${event.data.winner}!`);
    }

    private onTurnStarted(event: TurnStartedEvent): void {
        console.info(`\nTurn ${event.data.turn} started`);
    }
}
