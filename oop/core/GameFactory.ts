import { Game, Unit } from './Game';

interface Params {
    unitsCount: number;
}

export abstract class GameFactory {
    public createGame({ unitsCount }: Params): Game {
        const units = this.createUnits(unitsCount);

        const game = new Game(units);

        return game;
    }

    protected createUnits(count: number): Unit[] {
        const units: Unit[] = [];

        for (let i = 0, unit; i < count; i++) {
            unit = this.createUnit();
            units.push(unit);
        }

        return units;
    }

    protected abstract createUnit(): Unit;
}
