import { faker } from '@faker-js/faker';
import assert from 'assert';

import { EventLogger } from './EventLogger';
import { Event } from './event/Event';
import { EventPublisher } from './event/EventPublisher';
import { GameStartedEvent } from './event/GameStartedEvent';
import { GameEndedEvent } from './event/GameEndedEvent';
import { TurnStartedEvent } from './event/TurnStartedEvent';

const { TURN_DURATION = '300' } = process.env;

export interface Unit {
    isAlive(): boolean;
    attack(defender: Unit): void;
}

interface PickRandomUnitOptions {
    except?: Unit;
}

export class Game {
    private units: Unit[];
    private eventPublisher: EventPublisher = new EventPublisher();

    constructor(units: Unit[] = []) {
        this.units = units;
        new EventLogger().subscribeTo(this.eventPublisher);
    }

    public async start(): Promise<void> {
        assert(this.aliveUnits.length > 0, `To start the game alive units needed`);

        this.publish(new GameStartedEvent({ name: 'Highlander' }));

        let attacker: Unit;
        let defender: Unit;
    
        for (let turn = 1; this.aliveUnits.length > 1; turn += 1) {
            this.publish(new TurnStartedEvent({ turn }));

            attacker = this.pickRandomAliveUnit();
            defender = this.pickRandomAliveUnit({ except: attacker });
            attacker.attack(defender);

            await this.waitUntilNextTurn();
        }

        this.publish(new GameEndedEvent({ winner: this.aliveUnits[0] }));
    }

    private publish<T>(event: Event<T>): void {
        this.eventPublisher.publish(event);
    }

    private get aliveUnits(): Unit[] {
        return this.units.filter(unit => unit.isAlive());
    }

    private pickRandomAliveUnit({ except }: PickRandomUnitOptions = {}): Unit {
        const units = except ? this.aliveUnits.filter(unit => unit != except) : this.aliveUnits;

        return faker.helpers.arrayElement(units);
    }

    private async waitUntilNextTurn(): Promise<void> {
        const time = Number(TURN_DURATION);
        return new Promise((resolve) => setTimeout(resolve, time));
    }
}
