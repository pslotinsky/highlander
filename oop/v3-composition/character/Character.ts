import { faker } from '@faker-js/faker';
import assert from 'assert';

import { Unit } from '../../core/Game';
import { Race } from './race/Race';
import { Specialty } from './specialty/Specialty';

const MIN_LEVEL = 1;
const MAX_LEVEL = 5;

export class Character implements Unit {
    protected name: string = faker.name.fullName();
    protected level: number = 1;
    protected exp: number = 0;
    protected race: Race;
    protected specialty: Specialty;

    public constructor(race: Race, specialty: Specialty) {        
        this.race = race;
        this.specialty = specialty;

        console.info(`${this} created`);
    }

    public attack(victim: Character): void {
        console.info(`${this} attacked ${victim}`);

        assert(this.isAlive(), `Dead character can't attack`);
        assert(victim.isAlive(), `Dead character can't be attacked`);

        const damage = this.createDamage(victim);

        victim.suffer(damage);

        if (victim.isDead()) {
            console.info(`${victim} killed`);

            const exp = this.calculateExpForKilling(victim);
            this.addExp(exp);
        }
    }

    public isAlive(): boolean {
        return this.specialty.hp > 0;
    }

    public toString(): string {
        return `${this.race.name}-${this.specialty.constructor.name} ${this.name}`;
    }

    protected isDead(): boolean {
        return !this.isAlive();
    }

    protected createDamage(victim: Character): number {
        return this.race.getDamageMultiplierFor(victim.race) * this.specialty.createDamage();
    };

    protected suffer(rawDamage: number): void {
        const damage = this.reduceDamage(rawDamage);

        console.info(`${this} suffered ${damage} hp damage`);

        assert(damage >= 0, `Can't suffer negative damage`);

        this.specialty.hp = this.specialty.hp > damage ? this.specialty.hp - damage : 0;
    }

    protected reduceDamage(damage: number): number {
        return damage > this.defense ? Math.round(damage - this.defense) : 0;
    }

    protected addExp(value: number): void {
        if (this.level < MAX_LEVEL) {
            console.info(`${this} gained ${value} exp`);

            this.exp += value;

            if (this.exp >= this.nextLevelExp) {
                this.levelUp();
            }
        } else {
            console.info(`${this} already have max level`);
        }
    }

    protected levelUp(): void {
        this.specialty.hp = this.calculateHp(this.level);
        this.level += 1;

        console.info(`${this} reached ${this.level} level`);
    }
    
    protected calculateExpForKilling(character: Character): number {
        return Math.round(character.level / this.level * 100 + this.specialty.intelligence * 10);
    }

    protected calculateHp(level: number): number {
        return level * 50 + this.specialty.stamina * 10;
    }

    protected get defense(): number {
        return this.specialty.dexterity * 5 + this.specialty.stamina;
    }

    protected get nextLevelExp(): number {
        const levelExps = new Map([
            [1, 250],
            [2, 500],
            [3, 1000],
            [4, 2500],
            [5, 5000],
        ]);
        
        assert(this.level >= MIN_LEVEL, `Level can't be lesser then ${MIN_LEVEL}`);
        assert(this.level <= MAX_LEVEL, `Level can't be greater then ${MAX_LEVEL}`);

        return levelExps.get(this.level)!;
    }
}
