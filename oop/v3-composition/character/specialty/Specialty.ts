export abstract class Specialty {
    public abstract hp: number;
    public abstract strength: number;
    public abstract stamina: number;
    public abstract dexterity: number;
    public abstract intelligence: number;

    public abstract createDamage(): number;
}
