import { Specialty } from './Specialty';

export class Warrior extends Specialty {
    public hp: number = 750;
    public strength: number = 9;
    public stamina: number = 7;
    public dexterity: number = 3;
    public intelligence: number = 3;

    public createDamage(): number {
        return this.strength * 10 + this.dexterity * 5;
    }
}
