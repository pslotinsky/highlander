import { Specialty } from './Specialty';

export class Wizard extends Specialty {
    public hp: number = 500;
    public strength: number = 5;
    public stamina: number = 5;
    public dexterity: number = 5;
    public intelligence: number = 9;

    public createDamage(): number {
        return this.intelligence * 5 + this.dexterity * 5 + this.strength * 5;
    }
}
