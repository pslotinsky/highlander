import { Specialty } from './Specialty';

export class Ranger extends Specialty {
    public hp: number = 500;
    public strength: number = 5;
    public stamina: number = 5;
    public dexterity: number = 9;
    public intelligence: number = 5;

    public createDamage(): number {
        return this.dexterity * 10 + this.strength * 5;
    }
}
