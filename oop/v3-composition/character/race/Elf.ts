import { Race, RaceName } from './Race';

export class Elf extends Race {
    public name: RaceName = RaceName.Orc;

    public getDamageMultiplierFor(race: Race): number {
        return race.isDwarf() ? this.increasedDamageMultiplier : this.normalDamageMultiplier;
    };
}
