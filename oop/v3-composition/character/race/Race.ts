export enum RaceName {
    Orc = 'Orc',
    Elf = 'Elf',
    Dwarf = 'Dwarf',
}

export abstract class Race {
    protected readonly normalDamageMultiplier: number = 1;
    protected readonly increasedDamageMultiplier: number = 1.1;

    public abstract name: RaceName;

    public abstract getDamageMultiplierFor(race: Race): number;

    public isElf(): boolean {
        return this.name === RaceName.Elf;
    }

    public isOrc(): boolean {
        return this.name === RaceName.Orc;
    }

    public isDwarf(): boolean {
        return this.name === RaceName.Dwarf;
    }
}
