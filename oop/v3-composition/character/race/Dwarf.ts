import { Race, RaceName } from './Race';

export class Dwarf extends Race {
    public name: RaceName = RaceName.Orc;

    public getDamageMultiplierFor(race: Race): number {
        return race.isOrc() ? this.increasedDamageMultiplier : this.normalDamageMultiplier;
    };
}
