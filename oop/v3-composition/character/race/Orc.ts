import { Race, RaceName } from './Race';

export class Orc extends Race {
    public name: RaceName = RaceName.Orc;

    public getDamageMultiplierFor(race: Race): number {
        return race.isElf() ? this.increasedDamageMultiplier : this.normalDamageMultiplier;
    };
}
