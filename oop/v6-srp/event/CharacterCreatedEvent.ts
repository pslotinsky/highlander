import { Event } from '../../core/event/Event';
import { Character } from '../character/Character';

export class CharacterCreatedEvent extends Event<{ character: Character }> {}
