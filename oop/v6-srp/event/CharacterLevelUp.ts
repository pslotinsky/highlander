import { Event } from '../../core/event/Event';
import { Character } from '../character/Character';
import { Level } from '../character/Level';

export class CharacterLevelUp extends Event<{ character: Character, level: Level }> {}
