import { Event } from '../../core/event/Event';
import { Character } from '../character/Character';

export class CharacterAttackedEvent extends Event<{ attacker: Character, defender: Character, damage: number }> {}
