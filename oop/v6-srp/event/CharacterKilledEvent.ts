import { Event } from '../../core/event/Event';
import { Character } from '../character/Character';

export class CharacterKilledEvent extends Event<{ attacker: Character, defender: Character, exp: number }> {}
