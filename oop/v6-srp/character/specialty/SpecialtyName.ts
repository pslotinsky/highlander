export enum SpecialtyName {
    Ranger = 'Ranger',
    Warrior = 'Warrior',
    Wizard = 'Wizard',
}
