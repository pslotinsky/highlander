export enum RaceName {
    Orc = 'Orc',
    Elf = 'Elf',
    Dwarf = 'Dwarf',
}
