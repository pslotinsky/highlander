import { raceConfig, RaceConfigData } from '../../config';
import { AttackModifier } from '../attack/modifiers/AttackModifier';
import { RaceName } from './RaceName';

export class Race {
    public readonly name: RaceName;

    constructor(name: RaceName) {
        this.name = name;
    }

    public getAttackModifiers(): AttackModifier[] {
        return this.config.attackModifiers;
    }

    public hasName(name: RaceName): boolean {
        return this.name === name;
    }

    public toString(): string {
        return this.name;
    }

    private get config(): RaceConfigData {
        return raceConfig[this.name];
    }
}
