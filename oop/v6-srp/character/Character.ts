import { faker } from '@faker-js/faker';
import assert from 'assert';

import { Unit } from '../../core/Game';
import { Attack } from './attack/Attack';
import { AttackModifier } from './attack/modifiers/AttackModifier';
import { Level } from './Level';
import { Race } from './race/Race';
import { RaceName } from './race/RaceName';
import { Specialty } from './specialty/Specialty';
import { StatName, Stats } from './Stats';
import { EventPublisher } from '../../core/event/EventPublisher';
import { CharacterCreatedEvent } from '../event/CharacterCreatedEvent';
import { Event } from '../../core/event/Event';
import { CharacterAttackedEvent } from '../event/CharacterAttackedEvent';
import { CharacterKilledEvent } from '../event/CharacterKilledEvent';
import { CharacterLevelUp } from '../event/CharacterLevelUp';

export class Character implements Unit {
    protected name: string = faker.name.fullName();
    protected level: Level;
    protected stats: Stats;
    protected race: Race;
    protected specialty: Specialty;
    protected eventPublisher: EventPublisher = new EventPublisher();

    constructor(race: Race, specialty: Specialty) {        
        this.race = race;
        this.specialty = specialty;
        this.stats = specialty.createStats();
        this.level = new Level(this.stats);

        this.eventPublisher.subscribe(CharacterAttackedEvent, event => this.onAttacked(event));
        this.eventPublisher.subscribe(CharacterKilledEvent, event => this.onKill(event));

        this.eventPublisher.publish(new CharacterCreatedEvent({ character: this }));
    }

    public attack(victim: Character): void {
        const modifiers = this.race.getAttackModifiers();
        const attack = this.specialty.createAttack(victim, this, modifiers);

        attack.execute();
    }

    public isAlive(): boolean {
        return this.stats.isAlive();
    }

    public isDead(): boolean {
        return this.stats.isDead();
    }

    public hasRace(name: RaceName): boolean {
        return this.race.hasName(name);
    }

    public getStat(name: StatName): number {
        return this.stats.getStat(name);
    }

    public publish<T>(event: Event<T>): void {
        this.eventPublisher.publish(event);
    }

    public toString(): string {
        return `${this.race}-${this.specialty} ${this.name} (level ${this.level})`;
    }

    protected onAttacked(event: CharacterAttackedEvent): void {
        const { attacker, defender, damage } = event.data;

        this.stats.decreaseHpBy(damage);

        if (this.isDead()) {
            attacker.publish(new CharacterKilledEvent({
                attacker,
                defender,
                exp: attacker.level.getExpForKilling(defender.level)
            }));
        }
    }

    protected onKill(event: CharacterKilledEvent): void {
        this.level.addExp(event.data.exp, () => {
            this.publish(new CharacterLevelUp({
                character: this,
                level: this.level,
            }));
        });
    }
}
