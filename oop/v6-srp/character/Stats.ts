interface Params {
    hp: number;
    strength: number;
    stamina: number;
    dexterity: number;
    intelligence: number;
}

export type StatName = keyof Params; 

export class Stats {
    private params: Params;

    constructor(params: Params) {
        this.params = { ...params };
    }

    public getStat(name: StatName): number {
        return this.params[name];
    }

    public setStat(name: StatName, value: number) {
        this.params[name] = value;
    }

    public decreaseHpBy(damage: number): void {
        const { hp } = this.params;

        this.params.hp = hp > damage ? hp - damage : 0;
    }

    public calculateHp(level: number): number {
        const { stamina } = this.params;

        return level * 50 + stamina * 10;
    }

    public isAlive(): boolean {
        const { hp } = this.params;

        return hp > 0;
    }

    public isDead(): boolean {
        return !this.isAlive();
    }
}
