import assert from 'assert';

import { Stats } from './Stats';

const MIN_LEVEL = 1;
const MAX_LEVEL = 5;

export class Level {
    protected value: number = 1;
    protected exp: number = 0;
    protected stats: Stats;

    constructor(stats: Stats) {
        this.stats = stats;
    }

    public addExp(value: number, onLevelUp: () => void): void {
        if (this.value < MAX_LEVEL) {
            this.exp += value;

            if (this.exp >= this.maxExp) {
                this.levelUp();
                onLevelUp();
            }
        }
    }

    public getExpForKilling(level: Level): number {
        const intelligence = this.stats.getStat('intelligence');

        return Math.round(level.value / this.value * 100 + intelligence * 10);
    }

    public toString(): string {
        return this.value.toString();
    }

    private levelUp(): void {
        const hp = this.stats.calculateHp(this.value);
        this.stats.setStat('hp', hp)

        this.value += 1;
    }

    private get maxExp(): number {
        const levelExps = new Map([
            [1, 250],
            [2, 500],
            [3, 1000],
            [4, 2500],
            [5, 5000],
        ]);
        
        assert(this.value >= MIN_LEVEL, `Level can't be lesser then ${MIN_LEVEL}`);
        assert(this.value <= MAX_LEVEL, `Level can't be greater then ${MAX_LEVEL}`);

        return levelExps.get(this.value)!;
    }
}
