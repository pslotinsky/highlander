import { Attack } from './Attack';

export class MagicAttack extends Attack {
    public getRawDamage(): number {
        const intelligence = this.attacker.getStat('intelligence');
        const dexterity = this.attacker.getStat('dexterity');
        const strength = this.attacker.getStat('strength');

        return intelligence * 5 + dexterity * 5 + strength * 5;
    }
}
