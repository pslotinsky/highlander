import { Character } from '../../Character';

export abstract class AttackModifier {
    public abstract isApplicableFor(target: Character): boolean;
    public abstract applyOn(damage: number): number;
}
