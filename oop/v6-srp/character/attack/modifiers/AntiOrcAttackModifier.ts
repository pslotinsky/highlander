import { Character } from '../../Character';
import { RaceName } from '../../race/RaceName';
import { AttackModifier } from './AttackModifier';

export class AntiOrcAttackModifier extends AttackModifier {
    public isApplicableFor(target: Character): boolean {
        return target.hasRace(RaceName.Orc);
    }

    public applyOn(damage: number): number {
        return 1.1 * damage;
    }
}
