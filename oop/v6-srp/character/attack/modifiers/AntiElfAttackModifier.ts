import { Character } from '../../Character';
import { RaceName } from '../../race/RaceName';
import { AttackModifier } from './AttackModifier';

export class AntiElfAttackModifier extends AttackModifier {
    public isApplicableFor(target: Character): boolean {
        return target.hasRace(RaceName.Elf);
    }

    public applyOn(damage: number): number {
        return 1.1 * damage;
    }
}
