import { Attack } from './Attack';

export class RangedAttack extends Attack {
    public getRawDamage(): number {
        const dexterity = this.attacker.getStat('dexterity');
        const strength = this.attacker.getStat('strength');

        return dexterity * 10 + strength * 5;
    }
}
