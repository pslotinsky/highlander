import { Attack } from './Attack';

export class MeleeAttack extends Attack {
    public getRawDamage(): number {
        const strength = this.attacker.getStat('strength');
        const dexterity = this.attacker.getStat('dexterity');

        return strength * 10 + dexterity * 5;
    }
}
