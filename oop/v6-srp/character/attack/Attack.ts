import assert from 'assert';

import { CharacterAttackedEvent } from '../../event/CharacterAttackedEvent';
import { Character } from '../Character';
import { AttackModifier } from './modifiers/AttackModifier';

export abstract class Attack {
    public damage: number;

    protected defender: Character;
    protected attacker: Character;
    protected modifiers: AttackModifier[];

    constructor(target: Character, attacker: Character, modifiers: AttackModifier[]) {
        this.defender = target;
        this.attacker = attacker;
        this.damage = this.getRawDamage();
        this.modifiers = modifiers;
    }

    public execute(): void {
        assert(this.attacker.isAlive(), `Dead character can't attack`);
        assert(this.defender.isAlive(), `Dead character can't be attacked`);

        this.applyModifiers();
        this.applyDefense();

        this.defender.publish(new CharacterAttackedEvent({
            attacker: this.attacker,
            defender: this.defender,
            damage: this.damage,
        }));
    }

    public abstract getRawDamage(): number;

    private applyModifiers(): void {
        this.damage = this.modifiers
            .filter(modifier => modifier.isApplicableFor(this.defender))
            .reduce((damage, modifier) => modifier.applyOn(damage), this.damage);
    }

    private applyDefense(): void {
        const defense = this.calculateDefense();

        this.damage = this.damage > defense ? Math.round(this.damage - defense) : 0;
    }

    private calculateDefense(): number {
        const dexterity = this.defender.getStat('dexterity');
        const stamina = this.defender.getStat('stamina');

        return dexterity * 5 + stamina;
    }
}
