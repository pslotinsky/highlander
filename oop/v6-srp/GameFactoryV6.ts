import { faker } from '@faker-js/faker';

import { Unit } from '../core/Game';
import { GameFactory } from '../core/GameFactory';
import { Character } from './character/Character';
import { Race } from './character/race/Race';
import { RaceName } from './character/race/RaceName';
import { Specialty } from './character/specialty/Specialty';
import { SpecialtyName } from './character/specialty/SpecialtyName';

export class GameFactoryV6 extends GameFactory {
    protected createUnit(): Unit {
        const raceName = faker.helpers.arrayElement([RaceName.Dwarf, RaceName.Elf, RaceName.Orc]);
        const specialtyName = faker.helpers.arrayElement([SpecialtyName.Ranger, SpecialtyName.Warrior, SpecialtyName.Wizard]);
    
        return new Character(new Race(raceName), new Specialty(specialtyName));
    }
}
