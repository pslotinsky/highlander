import { MagicAttack } from './character/attack/MagicAttack';
import { MeleeAttack } from './character/attack/MeleeAttack';
import { AntiDwarfAttackModifier } from './character/attack/modifiers/AntiDwarfAttackModifier';
import { AntiElfAttackModifier } from './character/attack/modifiers/AntiElfAttackModifier';
import { AntiOrcAttackModifier } from './character/attack/modifiers/AntiOrcAttackModifier';
import { AttackModifier } from './character/attack/modifiers/AttackModifier';
import { RangedAttack } from './character/attack/RangedAttack';
import { RaceName } from './character/race/RaceName';
import { SpecialtyName } from './character/specialty/SpecialtyName';

export interface RaceConfigData {
    attackModifiers: AttackModifier[];
}

export interface SpecialtyConfigData {
    attackCtor: (typeof MeleeAttack | typeof RangedAttack | typeof MagicAttack);
    stats: {
        hp: number;
        strength: number;
        stamina: number;
        dexterity: number;
        intelligence: number;
    };
}

export const raceConfig: Record<RaceName, RaceConfigData> = {
    [RaceName.Dwarf]: {
        attackModifiers: [new AntiOrcAttackModifier()],
    },
    [RaceName.Elf]: {
        attackModifiers: [new AntiDwarfAttackModifier()],
    },
    [RaceName.Orc]: {
        attackModifiers: [new AntiElfAttackModifier()],
    },
};

export const specialtyConfig: Record<SpecialtyName, SpecialtyConfigData> = {
    [SpecialtyName.Ranger]: {
        stats:{
            hp: 500,
            strength: 5,
            stamina: 5,
            dexterity: 9,
            intelligence: 5,
        },
        attackCtor: RangedAttack,
    },
    [SpecialtyName.Warrior]: {
        stats: {
            hp: 750,
            strength: 9,
            stamina: 7,
            dexterity: 3,
            intelligence: 3,
        },
        attackCtor: MeleeAttack,
    },
    [SpecialtyName.Wizard]: {
        stats: {
            hp: 500,
            strength: 5,
            stamina: 5,
            dexterity: 5,
            intelligence: 9,
        },
        attackCtor: MagicAttack,
    },
};
