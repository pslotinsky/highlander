import { Unit } from '../core/Game';
import { GameFactory } from '../core/GameFactory';
import { Character } from './Character';

export class GameFactoryV0 extends GameFactory {
    protected createUnit(): Unit {
        return new Character();
    }
}
