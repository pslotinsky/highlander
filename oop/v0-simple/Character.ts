import { faker } from '@faker-js/faker';
import assert from 'assert';

import { Unit } from '../core/Game';

export class Character implements Unit {
    protected name: string = faker.name.fullName();
    protected hp: number = 500;

    public constructor() {
        console.info(`${this} created`);
    }

    public attack(victim: Character): void {
        console.info(`${this} attacked ${victim}`);

        assert(this.isAlive(), `Dead character can't attack`);
        assert(victim.isAlive(), `Dead character can't be attacked`);

        victim.suffer(100);

        if (victim.isDead()) {
            console.info(`${victim} killed`);
        }
    }

    public isAlive(): boolean {
        return this.hp > 0;
    }

    public toString(): string {
        return this.name;
    }

    protected isDead(): boolean {
        return !this.isAlive();
    }

    protected suffer(damage: number): void {
        console.info(`${this} suffered ${damage} hp damage`);

        assert(damage >= 0, `Can't suffer negative damage`);

        this.hp = this.hp > damage ? this.hp - damage : 0;
    }
}
