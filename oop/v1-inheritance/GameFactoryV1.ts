import { faker } from '@faker-js/faker';

import { Unit } from '../core/Game';
import { GameFactory } from '../core/GameFactory';
import { Ranger } from './character/specialty/Ranger';
import { Warrior } from './character/specialty/Warrior';
import { Wizard } from './character/specialty/Wizard';

export class GameFactoryV1 extends GameFactory {
    protected createUnit(): Unit {
        const ctor = faker.helpers.arrayElement([Warrior, Wizard, Ranger]);

        return new ctor();
    }
}
