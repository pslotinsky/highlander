import { faker } from '@faker-js/faker';
import assert from 'assert';

import { Unit } from '../../core/Game';

const MIN_LEVEL = 1;
const MAX_LEVEL = 5;

export abstract class Character implements Unit {
    protected name: string = faker.name.fullName();
    protected level: number = 1;
    protected exp: number = 0;
    protected abstract hp: number;
    protected abstract strength: number;
    protected abstract stamina: number;
    protected abstract dexterity: number;
    protected abstract intelligence: number;

    public constructor() {
        console.info(`${this} created`);
    }

    public attack(victim: Character): void {
        console.info(`${this} attacked ${victim}`);

        assert(this.isAlive(), `Dead character can't attack`);
        assert(victim.isAlive(), `Dead character can't be attacked`);

        const damage = this.createDamage();

        victim.suffer(damage);

        if (victim.isDead()) {
            console.info(`${victim} killed`);

            const exp = this.calculateExpForKilling(victim);
            this.addExp(exp);
        }
    }

    public isAlive(): boolean {
        return this.hp > 0;
    }

    public toString(): string {
        return `${this.constructor.name} ${this.name}`;
    }

    protected isDead(): boolean {
        return !this.isAlive();
    }

    protected abstract createDamage(): number;

    protected suffer(rawDamage: number): void {
        const damage = this.reduceDamage(rawDamage);

        console.info(`${this} suffered ${damage} hp damage`);

        assert(damage >= 0, `Can't suffer negative damage`);

        this.hp = this.hp > damage ? this.hp - damage : 0;
    }

    protected reduceDamage(damage: number): number {
        return damage > this.defense ? damage - this.defense : 0;
    }

    protected addExp(value: number): void {
        if (this.level < MAX_LEVEL) {
            console.info(`${this} gained ${value} exp`);

            this.exp += value;

            if (this.exp >= this.nextLevelExp) {
                this.levelUp();
            }
        } else {
            console.info(`${this} already have max level`);
        }
    }

    protected levelUp(): void {
        this.hp = this.calculateHp(this.level);
        this.level += 1;

        console.info(`${this} reached ${this.level} level`);
    }
    
    protected calculateExpForKilling(character: Character): number {
        return Math.round(character.level / this.level * 100 + this.intelligence * 10);
    }

    protected calculateHp(level: number): number {
        return level * 50 + this.stamina * 10;
    }

    protected get defense(): number {
        return this.dexterity * 5 + this.stamina;
    }

    protected get nextLevelExp(): number {
        const levelExps = new Map([
            [1, 250],
            [2, 500],
            [3, 1000],
            [4, 2500],
            [5, 5000],
        ]);
        
        assert(this.level >= MIN_LEVEL, `Level can't be lesser then ${MIN_LEVEL}`);
        assert(this.level <= MAX_LEVEL, `Level can't be greater then ${MAX_LEVEL}`);

        return levelExps.get(this.level)!;
    }
}
